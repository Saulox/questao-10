package Questao10;

public class BOOK {
	

	private String AUTHOR;
	private String EDITION;
	private String VOLUME;
	
	public BOOK(String AUTHOR) {
		this.AUTHOR = AUTHOR;
	}

	public String getEDITION() {
		return EDITION;
	}

	public void setEDITION(String eDITION) {
		EDITION = eDITION;
	}

	public String getAUTHOR() {
		return AUTHOR;
	}

	public void setAUTHOR(String aUTHOR) {
		AUTHOR = aUTHOR;
	}

	public String getVOLUME() {
		return VOLUME;
	}

	public void setVOLUME(String vOLUME) {
		VOLUME = vOLUME;
	}
	
	public void DISPLAY() {
		System.out.println("AUTHOR: "+AUTHOR+"\n"
				+ "EDITION: "+EDITION+"\n"
						+ "VOLUME: "+VOLUME+"\n");
				
	}
	
	
}
