package Questao10;

public class Itens {
	private String title;
	private String publisher;
	private String yearpublisher;
	private String ISBN;
	private String price;
	
	public Itens(String title) {
		this.title=title;
		
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getYearpublisher() {
		return yearpublisher;
	}
	public void setYearpublisher(String yearpublisher) {
		this.yearpublisher = yearpublisher;
	}
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String iSBN) {
		this.ISBN = iSBN;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public void DISPLAY() {
		System.out.println("TITLE: "+title+"\n"
				+ "PUBLISHER: "+publisher+"\n"
						+ "YEARPUBLISHER: "+yearpublisher+"\n"
								+ "ISBN: "+ISBN+"\n"
										+ "PRICE: "+price);
	}
	
	
}
